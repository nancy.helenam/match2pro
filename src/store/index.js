import Vue from 'vue'
import Vuex from 'vuex';
import { stat } from 'fs';

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        loggedUser: null,
        results: null
    },
    mutations: {
        addLoggedUser(state, loggedUser) {
            state.loggedUser = loggedUser;
        },
        results(state,results){
            state.results = results;
    }
}
    
});