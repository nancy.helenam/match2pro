import store from '../store';

function guard(to, from, next) {
  if (store.state.loggedUser) {
    next();
  } else {
    next('/login');
  }
}

const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/search', component: () => import('pages/search.vue') },
      {path: '/results',component: ()=> import('pages/results.vue')},
      {path: '/users',component: ()=> import('pages/users.vue')},
      {path: '/games',component: ()=> import('pages/games.vue')}
  
    ],

    beforeEnter: guard

  },
  {
    path: '/login',
    component: () => import('pages/Login.vue'),
  },
  {
    path: '/signUp',
    component: ()=> import('pages/signUp.vue'),
  }
  

  
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
